﻿using System.Web;
using System.Web.Mvc;

namespace http_5204_a_n01330654_passion_project
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
