﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Diagnostics;
using http_5204_a_n01330654_passion_project.Models;
using http_5204_a_n01330654_passion_project.Models.ViewModels;

namespace http_5204_a_n01330654_passion_project.Controllers
{
    public class GalleryController : Controller
    {
        private PixelSnapCMS db = new PixelSnapCMS();

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            IEnumerable<Gallery> galleries = db.Galleries.ToList();
            return View(galleries);
        }
        public ActionResult New()
        {
            GalleryEdit galleryeditview = new GalleryEdit();

            return View(galleryeditview);
        }
        [HttpPost]
        public ActionResult New(string GalleryName_New, string GalleryDescription_New)
        {
            string query = "insert into Galleries (GalleryName, GalleryDescription) values (@name, @description)";

            SqlParameter[] myparams = new SqlParameter[2];
            myparams[0] = new SqlParameter("@name", GalleryName_New);
            myparams[1] = new SqlParameter("@description", GalleryDescription_New);

            db.Database.ExecuteSqlCommand(query, myparams);
            return RedirectToAction("List");

        }public ActionResult Edit(int id)
        {
            GalleryEdit galleryeditview = new GalleryEdit();
            galleryeditview.gallery = db.Galleries.Find(id);
            return View(galleryeditview);
        }

        [HttpPost]
        public ActionResult Edit(int id, string GalleryName_New, string GalleryDescription_New)
        {
            if((id == null) || (db.Galleries.Find(id) == null))
            {
                return HttpNotFound();
            }
            string query = "update Galleries set GalleryName=@name, GalleryDescription=@description where GalleryID=@id";

            SqlParameter[] myparams = new SqlParameter[3];
            myparams[0] = new SqlParameter("@name", GalleryName_New);
            myparams[1] = new SqlParameter("@description", GalleryDescription_New);
            myparams[2] = new SqlParameter("@id", id);

            db.Database.ExecuteSqlCommand(query, myparams);
            return RedirectToAction("Show/"+id);

        }
        public ActionResult Show(int? id)
        {
    
            if ((id == null) || (db.Galleries.Find(id) == null))
            {
                return HttpNotFound();

            }
            string query = "select * from Galleries where GalleryID=@id";
           
            SqlParameter[] myparams = new SqlParameter[1];
            myparams[0] = new SqlParameter("@id", id);

            Gallery mygallery = db.Galleries.SqlQuery(query, myparams).FirstOrDefault();

            return View(mygallery);
        }

        public ActionResult Delete(int id)
        {
            string query = "delete from Galleries where GalleryID = @id";
            db.Database.ExecuteSqlCommand(query, new SqlParameter("@id", id));
            return RedirectToAction("List");
        }
    }
}