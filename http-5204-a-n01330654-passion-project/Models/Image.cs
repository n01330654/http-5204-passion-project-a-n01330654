﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace http_5204_a_n01330654_passion_project.Models
{
    public class Image
    {
        //imageID
        [Key, ScaffoldColumn(false)]
        public int ImageID { get; set; }

        //image Name
        [Required, StringLength(255), Display(Name = "Image Name")]
        public string ImageName { get; set; }

        //mage description
        [StringLength(int.MaxValue), Display(Name = "Image Description")]
        public string ImageDescription { get; set; }

        public int HasPic { get; set; }

        public string ImgType { get; set; }

        //one gallery to many images
        public virtual Gallery galleries { get; set; }
    }
}