﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace http_5204_a_n01330654_passion_project.Models.ViewModels
{
    public class ImageEdit
    {
        public ImageEdit()
        {

        }
        public virtual Image image { get; set; }

        public IEnumerable<Gallery> galleries { get; set; }

    }
}