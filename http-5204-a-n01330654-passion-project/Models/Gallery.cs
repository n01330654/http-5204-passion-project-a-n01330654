﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace http_5204_a_n01330654_passion_project.Models
{
    public class Gallery
    {
        //gallertID
        [Key,ScaffoldColumn(false)]
        public int GalleryID { get; set; }

        //gallery name
        [Required, StringLength(255), Display(Name = "Gallery Name")]
        public string GalleryName { get; set; }

        //gallery description
        [StringLength(int.MaxValue), Display(Name = "Gallery Description")]
        public string GalleryDescription { get; set; }

        //one gallery to many images
        public virtual ICollection<Image> Images { get; set; }

        

   

        
    }
}